package com.wraend.inftimetable.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.wraend.inftimetable.database.BuildingsTable;
import com.wraend.inftimetable.database.CoursesTable;
import com.wraend.inftimetable.database.DBHelper;
import com.wraend.inftimetable.database.RoomsTable;
import com.wraend.inftimetable.database.TimetableTable;

/**
 * TimetableContentProvider
 * Based in part on Android SQLite tutorial
 * available from vogella.com
 */

public class TimetableContentProvider extends ContentProvider {
	
	private static final String TAG = "TimetableContentProvider";
	private static final String AUTHORITY = "com.wraend.inftimetable.provider";
	
	private static final int COURSES = 1;
	private static final int BUILDINGS = 2;
	private static final int ROOMS = 3;
	private static final int TIMETABLE = 4;
	private static final int COURSES_ID = 5;
	//private static final int BUILDINGS_ID = 6;
	//private static final int ROOMS_ID = 7;
	
	private DBHelper dbHelper;
	
	public static final Uri COURSES_URI = Uri.parse("content://" + AUTHORITY  + "/courses");
	public static final Uri BUILDINGS_URI = Uri.parse("content://" + AUTHORITY  + "/buildings");
	public static final Uri ROOMS_URI = Uri.parse("content://" + AUTHORITY  + "/rooms");
	public static final Uri TIMETABLE_URI = Uri.parse("content://" + AUTHORITY  + "/timetable");


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
    	
      // handling for each table
      sURIMatcher.addURI(AUTHORITY, "courses", COURSES);
      sURIMatcher.addURI(AUTHORITY, "buildings", BUILDINGS);
      sURIMatcher.addURI(AUTHORITY, "rooms", ROOMS);
      sURIMatcher.addURI(AUTHORITY, "timetable", TIMETABLE);
      
      // handling cases where we need to address a specific row by its ID
      sURIMatcher.addURI(AUTHORITY, "courses/#", COURSES_ID);
      /*sURIMatcher.addURI(AUTHORITY, "buildings/#", BUILDINGS_ID);
      sURIMatcher.addURI(AUTHORITY, "rooms/#", ROOMS_ID);*/
      // rows in 'timetable' have no ID (we don't need one)
    }
    
	@Override
	public boolean onCreate() {
		dbHelper = new DBHelper(getContext());
		return false;
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		
		Log.d(TAG, "Query: " + uri);
		// set up database 
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		int uriType = sURIMatcher.match(uri);
				
		// set table to run query on
		switch (uriType) {
		case COURSES:
			queryBuilder.setTables(CoursesTable.NAME);
			break;
		case COURSES_ID:
			queryBuilder.setTables(CoursesTable.NAME);
			// use URI to set individual row we're looking for
			queryBuilder.appendWhere(CoursesTable.COLUMN_ID + "=" + uri.getLastPathSegment());
			break;
		case BUILDINGS:
			queryBuilder.setTables(BuildingsTable.NAME);
			break;
		case ROOMS:
			queryBuilder.setTables(RoomsTable.NAME);
			break;
		case TIMETABLE:
			queryBuilder.setTables(TimetableTable.NAME);
			break;
		default:
			break;
		}
		
		// run query
		Cursor cursor = queryBuilder.query(db, projection, selection,
		        selectionArgs, null, null, sortOrder);
		
		Log.d(TAG, "Found " + cursor.getCount() + " rows");
		
		// tell cursor where to look for changes
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);

	    return cursor;
	}
	
	
	@Override
	public int delete(Uri uri, String sel, String[] selArgs) {
		// set up database 
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
	    int uriType = sURIMatcher.match(uri);
	    int rowsDeleted = 0;
	    
	    // don't handle deleting individual items - only need to drop entire tables
	    switch (uriType) {
	    case COURSES:
	    	rowsDeleted = db.delete(CoursesTable.NAME, sel, selArgs);
	    	break;
	    case BUILDINGS:
	    	rowsDeleted = db.delete(BuildingsTable.NAME, sel, selArgs);
	    	break;
	    case ROOMS:
	    	rowsDeleted = db.delete(RoomsTable.NAME, sel, selArgs);
	    	break;
	    case TIMETABLE:
	    	rowsDeleted = db.delete(TimetableTable.NAME, sel, selArgs);
	    	break;
	    default:
	        throw new IllegalArgumentException("Invalid URI for operation 'delete': " + uri);
	    }
	    
	    return rowsDeleted;
	}

	@Override
	public String getType(Uri uri) {
		// ignore this, we don't need it
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues vals) {
		// set up database 
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
	    int uriType = sURIMatcher.match(uri);
	    long id = 0;
	    
	    switch (uriType) {
	    case COURSES:
	    	id = db.insert(CoursesTable.NAME, null, vals);
	    	return Uri.parse("courses/" + id);
	    case BUILDINGS:
	    	id = db.insert(BuildingsTable.NAME, null, vals);
	    	return Uri.parse("buildings/" + id);
	    case ROOMS:
	    	id = db.insert(RoomsTable.NAME, null, vals);
	    	return Uri.parse("rooms/" + id);
	    case TIMETABLE:
	    	id = db.insert(TimetableTable.NAME, null, vals);
	    	return Uri.parse("timetable/" + id);
	    default:
	        throw new IllegalArgumentException("Invalid URI for operation 'insert': " + uri);
	    }
	}

	@Override
	public int update(Uri uri, ContentValues vals, String sel, String[] selArgs) {
		// set up database 
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		
	    int uriType = sURIMatcher.match(uri);
	    int rowsUpdated = 0;
	    
	    /* we don't want anything to be updated other than individual courses 
	     * (when marking them as being on the user timetable)
	     */
	    switch (uriType) {
	    case COURSES_ID:
	        String id = uri.getLastPathSegment();
	        if (TextUtils.isEmpty(sel)) {
	          rowsUpdated = db.update(CoursesTable.NAME, 
	              vals,
	              CoursesTable.COLUMN_ID + "=" + id, 
	              null);
	        } else {
	          rowsUpdated = db.update(CoursesTable.NAME, 
	              vals,
	              CoursesTable.COLUMN_ID + "=" + id 
	              + " AND " 
	              + sel,
	              selArgs);
	        }
	    	break;
	    default:
	        throw new IllegalArgumentException("Invalid URI for operation 'update': " + uri);
	    }
	    
	    return rowsUpdated;
	}

}
