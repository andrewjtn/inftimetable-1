package com.wraend.inftimetable.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	public static final String TAG = "DBHelper";	
	
	private static final String DB_NAME = "timetable.db";
	private static final int DB_VERSION = 1;
	
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CoursesTable.CREATE_TABLE);
		db.execSQL(BuildingsTable.CREATE_TABLE);
		db.execSQL(RoomsTable.CREATE_TABLE);
		db.execSQL(TimetableTable.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int v0, int v1) {
		Log.w(TAG, "Upgrading database from version " + v0 + " to "
		            + v1 + " - all data will be removed");
		
		// drop all tables
		db.execSQL("DROP TABLE IF EXISTS " + CoursesTable.NAME);
		db.execSQL("DROP TABLE IF EXISTS " + BuildingsTable.NAME);
		db.execSQL("DROP TABLE IF EXISTS " + RoomsTable.NAME);
		db.execSQL("DROP TABLE IF EXISTS " + TimetableTable.NAME);

		// recreate tables
		onCreate(db);		
	}

}
