package com.wraend.inftimetable.database;


public class TimetableTable {
	
	public static final String TAG = "TimetableHelper";

	public static final String NAME = "timetable";

	public static final String COLUMN_COURSE = "course";
	public static final String COLUMN_BUILDING = "building";
	public static final String COLUMN_ROOM = "room";
	public static final String COLUMN_SEMESTER = "semester";
	public static final String COLUMN_DAYOFWEEK = "dayofweek";
	public static final String COLUMN_STARTTIME = "starttime";
	public static final String COLUMN_FINISHTIME = "finishtime";
	public static final String COLUMN_YEARS = "years";
	public static final String COLUMN_COMMENT = "comment";
	
	public static final String CREATE_TABLE =
			"CREATE TABLE " + NAME + " "
			+ "(" + COLUMN_COURSE + " TEXT NOT NULL,"
			+ COLUMN_BUILDING + " TEXT,"
			+ COLUMN_ROOM + " TEXT,"
			+ COLUMN_SEMESTER + " INTEGER NOT NULL,"
			+ COLUMN_DAYOFWEEK + " TEXT NOT NULL,"
			+ COLUMN_STARTTIME + " TEXT NOT NULL,"
			+ COLUMN_FINISHTIME + " TEXT NOT NULL,"
			+ COLUMN_YEARS + " TEXT,"
			+ COLUMN_COMMENT + " TEXT)";

}
