package com.wraend.inftimetable.database;


public class RoomsTable {

	public static final String TAG = "RoomsHelper";
	
	public static final String NAME = "rooms";
	
	// name columns
	public static final String COLUMN_NAME = "_id";
	public static final String COLUMN_DESCRIPTION = "description";
		
	public static final String CREATE_TABLE =
			"CREATE TABLE " + NAME + " "
			+ "(" + COLUMN_NAME + " TEXT PRIMARY KEY,"
			+ COLUMN_DESCRIPTION + " TEXT NOT NULL)";
	
}
