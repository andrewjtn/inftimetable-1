package com.wraend.inftimetable;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.wraend.inftimetable.database.BuildingsTable;
import com.wraend.inftimetable.database.CoursesTable;
import com.wraend.inftimetable.database.RoomsTable;
import com.wraend.inftimetable.model.TimetableEntry;
import com.wraend.inftimetable.provider.TimetableContentProvider;

public class MyTimetableAdapter extends BaseExpandableListAdapter {
	
	private Context _context;
	// groups
    private List<String> _listDataHeader;
    // children, in form <group, [items]>
    private HashMap<String, List<TimetableEntry>> _listDataChild;
	
	public MyTimetableAdapter(Context context, List<String> listDataHeader,
            HashMap<String, List<TimetableEntry>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

	@Override
	public Object getChild(int groupPos, int childPos) {
		return this._listDataChild.get(this._listDataHeader.get(groupPos))
                .get(childPos);
	}

	@Override
	public long getChildId(int groupPos, int childPos) {
		return childPos;
	}

	@Override
	public View getChildView(int groupPos, int childPos, boolean isLastChild, View convertView,
			ViewGroup parent) {
		
		 final TimetableEntry entry = (TimetableEntry) getChild(groupPos, childPos);
		 
	     if (convertView == null) {
	    	 LayoutInflater infalInflater = (LayoutInflater) this._context
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	         convertView = infalInflater.inflate(R.layout.list_group_item_multicourse, null);
	        }
	    
	     
	     String[] bProjection = { BuildingsTable.COLUMN_DESCRIPTION };
	     Cursor cBuilding = _context.getContentResolver()
    		 					.query(TimetableContentProvider.BUILDINGS_URI, 
    		 							bProjection, BuildingsTable.COLUMN_NAME + "='" + entry.building + "'" , 
    		 							null, null);
	     
	     String[] rProjection = { RoomsTable.COLUMN_DESCRIPTION };
	     Cursor cRoom = _context.getContentResolver()
    		 					.query(TimetableContentProvider.ROOMS_URI, 
    		 							rProjection, RoomsTable.COLUMN_NAME + "='" + entry.room + "'" , 
    		 							null, null);
	     
	     String[] cProjection = { CoursesTable.COLUMN_NAME };
	     Cursor cCourse = _context.getContentResolver()
    		 					.query(TimetableContentProvider.COURSES_URI, 
    		 							cProjection, CoursesTable.COLUMN_ACRONYM + "='" + entry.course + "'" , 
    		 							null, null);
	     
	   
	     String bname;
	     String rname;
	     String cname;
	     
	     if (!(cBuilding.moveToFirst()) || cBuilding.getCount() == 0) {
	    	 bname = entry.building; 
	     } else {
	    	 bname = cBuilding.getString(0);
	     }
	     
	     if (!(cRoom.moveToFirst()) || cRoom.getCount() == 0) {
	    	 rname = entry.room;
	     } else {
	         rname = cRoom.getString(0);
	     }
	     
	     if (!(cCourse.moveToFirst()) || cCourse.getCount() == 0) {
	    	 cname = entry.course;
	     } else {
	         cname = cCourse.getString(0);
	     }
	     
        TextView entryTime = (TextView) convertView.findViewById(R.id.entry_time);
        TextView entryLocation = (TextView) convertView.findViewById(R.id.entry_location);
        TextView entryComment = (TextView) convertView.findViewById(R.id.entry_comment);
        TextView entryCourse = (TextView) convertView.findViewById(R.id.entry_course);

 
        entryTime.setText(entry.startTime + "-" + entry.finishTime);
        entryLocation.setText(rname + " - " + bname);
        entryCourse.setText(cname);
    
        if (!TextUtils.isEmpty(entry.comment)) {
        	entryComment.setText(entry.comment);
        	entryComment.setVisibility(0);
        }
        return convertView;
	}

	@Override
	public int getChildrenCount(int groupPos) {
		return this._listDataChild.get(this._listDataHeader.get(groupPos))
                .size();
	}

	@Override
	public Object getGroup(int groupPos) {
		return this._listDataHeader.get(groupPos);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPos) {
		return groupPos;
	}

	@Override
	public View getGroupView(int groupPos, boolean isExpanded, View convertView, ViewGroup parent) {
	   String headerTitle = (String) getGroup(groupPos);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_header, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.list_header);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// ignore, we're not using selectable lists
		return false;
	}

}
