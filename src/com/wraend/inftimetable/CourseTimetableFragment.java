package com.wraend.inftimetable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.wraend.inftimetable.database.TimetableTable;
import com.wraend.inftimetable.model.TimetableEntry;
import com.wraend.inftimetable.provider.TimetableContentProvider;

public class CourseTimetableFragment extends Fragment {

	private static final String TAG = "CourseTimetableFragment";
	
	public static String COURSE = "course";
	
	private CourseTimetableAdapter ctAdapter;
	private ExpandableListView ctList;
	private List<String> listDataHeader;
	private HashMap<String, List<TimetableEntry>> listDataChild;
	private String cAcronym;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View V = inflater.inflate(R.layout.fragment_course_timetable, container, false);
		ctList = (ExpandableListView) V.findViewById(R.id.course_timetable_list);
	
		Bundle args = this.getArguments();
		cAcronym = args.getString(COURSE);
		
		return V;    
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		ContentResolver resolver = this.getActivity().getContentResolver();
		
		String[] projection = { TimetableTable.COLUMN_STARTTIME, TimetableTable.COLUMN_FINISHTIME,
								TimetableTable.COLUMN_DAYOFWEEK, TimetableTable.COLUMN_BUILDING,
								TimetableTable.COLUMN_ROOM, TimetableTable.COLUMN_SEMESTER, 
								TimetableTable.COLUMN_COMMENT };
		Cursor c = resolver.query(TimetableContentProvider.TIMETABLE_URI, projection, 
						TimetableTable.COLUMN_COURSE + "='" + cAcronym + "'",
						null, null);
		
		selectCourse(c);
	}
	
	public void selectCourse(Cursor c) {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<TimetableEntry>>();

		c.moveToFirst();
		
		while (c.isAfterLast() == false) {
			
			String dayOfWeek = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_DAYOFWEEK));
			String startTime = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_STARTTIME));
			String finishTime = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_FINISHTIME));
			String room = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_ROOM));
			String building = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_BUILDING));
			Integer semester = c.getInt(c.getColumnIndexOrThrow(TimetableTable.COLUMN_SEMESTER));
			String comment = c.getString(c.getColumnIndexOrThrow(TimetableTable.COLUMN_COMMENT));
			
			TimetableEntry entry = new TimetableEntry(cAcronym, building, room, semester, dayOfWeek,
														startTime, finishTime, null, comment);

			// check if this lecture is on a day which we don't yet know
			if (listDataChild.get(dayOfWeek) == null) {
				// create empty list for this day of week and add the current lecture
				List<TimetableEntry> list = new ArrayList<TimetableEntry>();
				list.add(entry);
				
				// add this day of week to the data
				listDataChild.put(dayOfWeek, list);
			} else {
				// get this day of week's list of lectures and add this lecture
				List<TimetableEntry> list = listDataChild.get(dayOfWeek);
				list.add(entry);
				
				// replace this day of week's data
				listDataChild.put(dayOfWeek, list);				
			}
		
			Log.d(TAG, "Lecture starting on " + c.getString(2) + " at " + c.getString(0) + " and ending at " + c.getString(1));
			c.moveToNext();
		}
		
		// add each day which has lectures as a header
		for (String s : listDataChild.keySet()) {
			listDataHeader.add(s);
			List<TimetableEntry> entries = listDataChild.get(s);
			Collections.sort(entries, TIME_OF_DAY_ORDER);
			listDataChild.put(s, entries);
		}
		
		Collections.sort(listDataHeader, DAY_OF_WEEK_ORDER);
		ctAdapter = new CourseTimetableAdapter(getActivity(), listDataHeader, listDataChild);
		ctList.setAdapter(ctAdapter);	
		
		int count = ctAdapter.getGroupCount();
		for (int position = 1; position <= count; position++) {
		    ctList.expandGroup(position - 1);
		}

		//Log.d(TAG, "Going to try finding " + TimetableTable.COLUMN_COURSE + " = "+ courseAcronym);
		
		//String[] projection = { CoursesTable.COLUMN_ACRONYM };

		//resolver.query(TimetableContentProvider.COURSES_URI, projection, 
									
		/*while (c.isAfterLast() == false) {
			Log.d(TAG, "Lecture starts at " + c.getString(0) + " and ends at " + c.getString(1));
			c.moveToNext();
		}*/
		
	}
	
	 static final Comparator<String> DAY_OF_WEEK_ORDER = 
             new Comparator<String>() {		
 		public int compare(String day1, String day2) {
 			HashMap<String,Integer> days = new HashMap<String,Integer>();
 			days.put("Monday", 0);
 			days.put("Tuesday", 1);
 			days.put("Wednesday", 2);
 			days.put("Thursday", 3);
 			days.put("Friday", 4);
 			
 			// if there are days we don't know about just stick them on the end
 			if (days.get(day1) == null) {
 				days.put(day1, 5);
 			}
 			
 			if (days.get(day2) == null) {
 				days.put(day2, 6);
 			}
 			
 			return days.get(day1).compareTo(days.get(day2));
   		}
 		
	 };
	 
	 static final Comparator<TimetableEntry> TIME_OF_DAY_ORDER = 
             new Comparator<TimetableEntry>() {		
 		public int compare(TimetableEntry entry1, TimetableEntry entry2) {
 		
 			// compare the hours of two start times
 			// comparison doesn't need to be better than this under the common university timetable
 			Integer startHour1 = Integer.parseInt(entry1.startTime.substring(0, 2));
 			Integer startHour2 = Integer.parseInt(entry2.startTime.substring(0, 2));

 			return startHour1.compareTo(startHour2);
   		}
 		
	 };
	
}
