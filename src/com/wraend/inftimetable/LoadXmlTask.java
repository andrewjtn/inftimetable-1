package com.wraend.inftimetable;

import java.io.IOException;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.database.SQLException;
import android.os.AsyncTask;
import android.util.Log;

import com.wraend.inftimetable.database.DataWriter;
import com.wraend.inftimetable.model.Building;
import com.wraend.inftimetable.model.Course;
import com.wraend.inftimetable.model.Room;
import com.wraend.inftimetable.model.TimetableEntry;
import com.wraend.inftimetable.model.VenueCollection;
import com.wraend.inftimetable.xml.XmlLoader;

public class LoadXmlTask extends AsyncTask<String, Void, String> {
	
	private static final String TAG = "LoadXmlTask";
	private Context context;
	private OnTaskCompleted listener;
	private DataWriter writer;	
	
	private Exception ex = null;
	
	public LoadXmlTask(Context c, OnTaskCompleted listener) {
		this.context = c;
		this.listener = listener;
	}

	@Override
	protected String doInBackground(String... arg0) {
		List<Course> courses = null;
		VenueCollection venues = null;
		List<TimetableEntry> timetable = null;
		
		
		XmlLoader xml = new XmlLoader();
		try {
			courses = xml.loadCourses();
			venues = xml.loadVenues();
			timetable = xml.loadTimetable();
		} catch (XmlPullParserException e) {
			Log.e(TAG, "XML parsing problem: " + e.getMessage());
			// pass exception back to UI
			ex = e;
		} catch (IOException e) {
			Log.e(TAG, "IO error: " + e.getMessage());
			// pass exception back to UI
			ex = e;
		}
		
		writer = new DataWriter(this.context);
		writer.clearAll();
			
		/*
		 * don't try adding stuff to the database if the task has been
		 * cancelled
		 */
		if (ex == null) {
			
			// add all courses to database
			for (final Course c : courses) {
				//Log.d(TAG, "Course: " + c.acronym + ", Points: " + c.points);
				try {
					writer.addCourse(c);
				} catch (SQLException e) {
					Log.w(TAG, "SQL error: " + e.getMessage());
				}
			}
			
			// add all buildings to database
			for (final Building b : venues.buildings) {
				try {
					writer.addBuilding(b);
				} catch (SQLException e) {
					Log.w(TAG, "SQL error: " + e.getMessage());
				}
			}
			
			// add all rooms to database
			for (final Room r : venues.rooms) {
				try {
					writer.addRoom(r);
				} catch (SQLException e) {
					Log.w(TAG, "SQL error: " + e.getMessage());
				}
			}
			
			// add all timetable entries to database
			for (final TimetableEntry t : timetable) {
				try {
					writer.addTimetableEntry(t);
				} catch (SQLException e) {
					Log.w(TAG, "SQL error: " + e.getMessage());
				}
			}
		}
		
		Log.d(TAG, "XML parsing complete"); // we hope
		
		return null;
	}

	protected void onPostExecute(String x) {
		// tell the UI we're done unless something broke
		if (ex == null) {
			listener.onTaskCompleted();
		}
		else {
			listener.onTaskException(ex);
		}
	}
}
