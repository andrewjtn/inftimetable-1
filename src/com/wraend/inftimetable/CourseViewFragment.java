package com.wraend.inftimetable;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.wraend.inftimetable.database.CoursesTable;
import com.wraend.inftimetable.provider.TimetableContentProvider;

public class CourseViewFragment extends ListFragment implements OnTaskCompleted, LoaderManager.LoaderCallbacks<Cursor> {
	private static String TAG = "CourseViewFragment";
	
	private static final int INITIAL_LOAD = 0;
	private static final int REFRESH_LOAD = 1;
	
	private SimpleCursorAdapter adapter;
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		setEmptyText(getString(R.string.no_courses));
		setHasOptionsMenu(true);
		fillData(INITIAL_LOAD);

	}

	public void onRefresh() {
		Log.d(TAG, "Refresh button pressed");
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage("This will delete your custom timetable.")
			   .setTitle("Refresh data?");
		builder.setPositiveButton("Refresh",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
        		Log.d(TAG, "Refresh confirmed: will reload all data now");
            	setListAdapter(null);
            	setListShown(false);
                fetchData();
            	dialog.dismiss();
            }
        });
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id) {
	            	// do nothing
	        		Log.d(TAG, "Refresh cancelled: will not reload data");
	                dialog.cancel();
	            }
	    });		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void fillData(int loadContext) {
		
		// we want the course name and acronym to be displayed in each row
		String[] from = new String[] { CoursesTable.COLUMN_ACRONYM, CoursesTable.COLUMN_NAME };
		int[] to = new int[] { android.R.id.text2, android.R.id.text1 };
		
		// ask the loader manager to start up the database loader
	    getLoaderManager().initLoader(loadContext, null, this);
	    
	    // set how the database contents will be displayed in the list
		adapter = new SimpleCursorAdapter(this.getActivity(), android.R.layout.simple_list_item_2, null, from,
			        to, 0);
		
		//setListAdapter(adapter);
	}
	
	private void fetchData() {
		new LoadXmlTask(this.getActivity(), this).execute("");
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    Intent i = new Intent(getActivity(), SingleCourseActivity.class);
	    Uri uri = Uri.parse(TimetableContentProvider.COURSES_URI + "/" + id);
	    i.putExtra("com.wraend.inftimetable.course", uri);
	    startActivity(i);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.course_view, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
        case R.id.action_refresh:
        	onRefresh();
        	return true;
        default:
            return super.onOptionsItemSelected(item);
	    }
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Log.d(TAG, "Created loader. Attempting to query " + TimetableContentProvider.COURSES_URI);
		String[] proj = { CoursesTable.COLUMN_ID, CoursesTable.COLUMN_ACRONYM, CoursesTable.COLUMN_NAME };
		CursorLoader loader = new CursorLoader(this.getActivity(), TimetableContentProvider.COURSES_URI, proj, null, null, null);
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		Log.d(TAG, "Loader finished");
		
		int loaderId = loader.getId();
		boolean isEmpty = (cursor == null || cursor.getCount() == 0);
		
		if (isEmpty) {
			switch (loaderId) {
			case INITIAL_LOAD:
				// no data exists so we need to fetch XML
				// don't swap in any data (progress bar continues)
				Log.d(TAG, "No courses in database. Will now fetch XML");
				fetchData();
				break;
			case REFRESH_LOAD:
				// no data exists after XML fetched
				// swap in null (triggers empty message)
				Log.d(TAG, "No courses in database after XML was fetched.");
				adapter.swapCursor(null);
				setListAdapter(adapter);
				break;
			default:
				break;
			}	
		}
		
		// we have data - swap it in
		else {
			adapter.swapCursor(cursor);
			setListAdapter(adapter);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		Log.d(TAG, "Loader reset");
		adapter.swapCursor(null);
	}

	@Override
	public void onTaskCompleted() {
		Log.d(TAG, "Task completed");
		fillData(REFRESH_LOAD);		
	}

	@Override
	public void onTaskException(Exception e) {
		Log.d(TAG, "An exception occurred in the XML loader task");
    	setListAdapter(null);
    	
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage("There was a problem fetching data.");
		builder.setPositiveButton("Retry",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
        		Log.d(TAG, "Retry pressed: will now attempt to reload all data");
            	setListShown(false);
                fetchData();
            	dialog.dismiss();
            }
        });
		builder.setNegativeButton("OK",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
        		Log.d(TAG, "OK pressed: will not make further attempt to reload data");
            	// do nothing
                dialog.cancel();
            }
	    });
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
}
