package com.wraend.inftimetable.xml;

/**
 * CoursesParser
 * 
 * Based in part on code examples available from 
 * Android Open Source Project documentation 
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;

import com.wraend.inftimetable.model.Course;

public class CoursesParser extends XmlParser {
	private static final String TAG = "CoursesParser";
	
    public List<Course> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            Log.d(TAG, "Parsing courses");
            return readList(parser);
        } finally {
            in.close();
        }
    }
    
    private List<Course> readList(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Course> courses = new ArrayList<Course>();

        parser.require(XmlPullParser.START_TAG, ns, "list");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("course")) {
                courses.add(readCourse(parser));
            } else {
                skip(parser);
            }
        }  
        return courses;
    }
    
    private Course readCourse(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "course");
        String name = null;
        String url = null;
        String acronym = null;
        String lecturer = null;
        Integer year = null;
        Integer level = null;
        Integer points = null;
        String deliveryperiod = null;
        String euclid = null;
        String drps = null;
        boolean cs = false;
        boolean se = false;
        boolean ai = false;
        boolean cg = false;
        
        
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            if (tag.equals("name")) {
               	name = readTagText("name",parser);
            } else if (tag.equals("url")) {
                url = readTagText("url", parser);
            } else if (tag.equals("acronym")) {
                acronym = readTagText("acronym", parser);
            } else if (tag.equals("lecturer")) {
            	lecturer = readTagText("lecturer", parser);
            } else if (tag.equals("year")) {
            	year = Integer.valueOf(readTagText("year", parser));
            } else if (tag.equals("level")) {
            	level = Integer.valueOf(readTagText("level", parser));
            } else if (tag.equals("points")) {
            	points = Integer.valueOf(readTagText("points", parser));
            } else if (tag.equals("deliveryperiod")) {
            	deliveryperiod = readTagText("deliveryperiod", parser);
            } else if (tag.equals("drps")) {
            	drps = readTagText("drps", parser);
            } else if (tag.equals("euclid")) {
            	euclid = readTagText("euclid", parser);
            
            // FIXME: these always parse 'true' if tag exists
            } else if (tag.equals("cs") && !tag.isEmpty()) {
            	cs = true;
            	skip(parser);
            } else if (tag.equals("se") && !tag.isEmpty()) {
            	se = true;
            	skip(parser);
            } else if (tag.equals("ai") && !tag.isEmpty()) {
            	ai = true;
            	skip(parser);
            } else if (tag.equals("cg") && !tag.isEmpty()) {
            	cg = true;
            	skip(parser);
            	
            } else {
            	skip(parser);
            }
        }
        return new Course(name, url, acronym, lecturer, year, level, points, deliveryperiod, drps, euclid, cs, se, ai, cg);
    }
}
