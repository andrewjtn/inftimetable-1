package com.wraend.inftimetable.xml;

import android.util.Log;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.wraend.inftimetable.model.Room;
import com.wraend.inftimetable.model.Building;
import com.wraend.inftimetable.model.VenueCollection;


/**
 * VenuesParser
 * 
 * Based in part on code examples available from 
 * Android Open Source Project documentation 
 */

public class VenuesParser extends XmlParser {
	private static final String TAG = "VenuesParser";
    private static final String ns  = null;
	
	public VenueCollection parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            Log.d(TAG, "Parsing venues");
            return readVenues(parser);
        } finally {
            in.close();
        }
    }
	
	public VenueCollection readVenues(XmlPullParser parser) throws XmlPullParserException, IOException {
		List<Building> buildings = new ArrayList<Building>();
		List<Room> rooms = new ArrayList<Room>();
		
		parser.require(XmlPullParser.START_TAG, ns, "venues");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("building")) {
                buildings.add(readBuilding(parser));
            } else if (name.equals("room")) {
            	rooms.add(readRoom(parser));            
            } else {
                skip(parser);
            }
        }
        
		return new VenueCollection(buildings, rooms);
	}
	
	private Building readBuilding(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "building");
        String name = null;
        String description = null;
        String mapUrl = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            if (tag.equals("name")) {
               	name = readTagText("name",parser);
            } else if (tag.equals("description")) {
                description = readTagText("description", parser);
            } else if (tag.equals("map")) {
                mapUrl = readTagText("map",parser);
            } else {
                skip(parser);
            }
        }
        return new Building(name, description, mapUrl);
    }
	
	private Room readRoom(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "room");
        String name = null;
        String description = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String tag = parser.getName();
            if (tag.equals("name")) {
               	name = readTagText("name", parser);
            } else if (tag.equals("description")) {
                description = readTagText("description", parser);
            } else {
                skip(parser);
            }
        }
        return new Room(name, description);
    }
}
