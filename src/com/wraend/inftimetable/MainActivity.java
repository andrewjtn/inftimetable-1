package com.wraend.inftimetable;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends ActionBarActivity {

	private static final String TAG = "MainActivity";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG,"Hello world");
        super.onCreate(savedInstanceState);
        
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        
        Tab tab = actionBar.newTab()
                .setText("Timetable")
                .setTabListener(new TabListener<MyTimetableFragment>(
                        this, "mytt", MyTimetableFragment.class));
        actionBar.addTab(tab);

        tab = actionBar.newTab()
            .setText("Courses")
            .setTabListener(new TabListener<CourseViewFragment>(
                    this, "courses", CourseViewFragment.class));
        actionBar.addTab(tab);
        
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
