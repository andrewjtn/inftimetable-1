package com.wraend.inftimetable.model;

public class Building {
    public final String name;
    public final String description;
    public final String mapUrl;

    public Building(String name, String description, String mapUrl) {
        this.name = name;
        this.description = description;
        this.mapUrl = mapUrl;
    }
}