package com.wraend.inftimetable.model;

import java.util.List;

public final class VenueCollection {
	public final List<Building> buildings;
	public final List<Room> rooms;
	
	public VenueCollection(List<Building> buildings, List<Room> rooms) {
		this.buildings = buildings;
		this.rooms = rooms;
	}
}